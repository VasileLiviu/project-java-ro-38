package ex.school;

public class Person {

    long cnp;

    String name;

    String surname;

    int age;

    public Person(long cnp, String name, String surname, int age) {
        this.cnp=cnp;
        this.name=name;
        this.surname=surname;
        this.age=age;
    }

    public long getCnp() {
        return cnp;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public void setCnp(long cnp) {
        this.cnp = cnp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "cnp: " + cnp +
                ", name: '" + name + '\'' +
                ", surname: '" + surname + '\'' +
                ", age: " + age +
                '}';
    }
}
