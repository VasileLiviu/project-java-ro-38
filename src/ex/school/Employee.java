package ex.school;

public class Employee extends Person {
    private String ocupation;

    public Employee(String name, String surname, long cnp, int age, String ocupation){
        super(cnp, name, surname, age);
        this.ocupation=ocupation;
    }
}
