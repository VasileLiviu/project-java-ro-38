package ex.school;

public class Student extends Person{

    private long cnp;
    private String name;
    private String surname;
    private int age;
    private double examGrade;

    public Student(long cnp, String name, String surname, int age, double examGrade) {
        this.cnp = cnp;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.examGrade = examGrade;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public long getCnp(){
        return this.cnp;
    }

    public double getExamGrade() {
        return examGrade;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setCnp(long cnp) {
        this.cnp = cnp;
    }

    public void setExamGrade(double examGrade) {
        this.examGrade = examGrade;
    }

    @Override
    public String toString() {
        return "Student{" +
                "cnp=" + cnp +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", examGrade=" + examGrade +
                '}';
    }
}
