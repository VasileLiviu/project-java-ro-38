package ex.school;

public class Teacher extends Person {
    private String subject;

    public Teacher(String name, String surname, long cnp, int age, String subject) {
        super(cnp, name, surname, age);
        this.subject = subject;

    }
}
